/* En este programa va a pedir diferentes datos de tipo numerico al usuario para guardarlos en las variables correspondientes,
   realizara una operacion que generara un resultado para presentar
   ese valor como otro dato de salida, y poderlo imprimir en la pantalla. */

/* @author Rodriguez Soto Cristian El�as. */

/* @date 18 de marzo de 2021. */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num1, num2, num3, num4, num5, multiplicacion = 0; /* La mayoria de los numero los puse de forma entera para que no haya mucho problema al hacer la operacion. */
    float num6, num7, suma = 0; /* Coloque una suma de numeros decimales, para que se pueda aplicar lo que vi en la practica 2 */

    printf("Ingrese dos numeros enteros, para efectuar una multiplicacion: \n"); /* aqui doy una instruccion para hacer la operacion  */

    scanf("%i %i", &num1, &num2); /* la variable num1 y num2 almacenan los valores para despues utilizarlo en la operacion. */

    multiplicacion = num1 * num2; /* la operacion multiplicacion es la que se ejecuta tomando los vaalores de las variables. */

    printf("La multiplicacion de tus dos numeros es: %i \n \n", multiplicacion); /* aqui se imprimira el resultado de la multiplicacion. */

    printf("Ingrese dos numeros decimales, paaraaa efectuar una suma: \n"); /* aqui doy instrucciones para que la suma */

    scanf("%f %f", &num6, &num7); /* al igual que hace tiempo, pedi valores de numeros enteros, ahora los numeros decimales se guardan aqui. */

    suma = num6 + num7; /* se ejecutaa la suma, con los datos guardados dentro de las variables */

    printf("La suma de tus dos numeros son: %f \n", suma); /* imprime el resultado de la suma. */

    printf("\n"); /* es un salto de linea, para poder dar presentacion. */

    printf("En este punto te pedira 5 numeros, si cumplen con lo que dicen, aparecera 1 (es verdadero) o un 0 (si es falso) \n \n"); /* aqui inicia la instruccion para hacer la siguiente operacion */

    printf("Escriba el primer numero entero: "); /* pide los valores que seran almacenados en la variable correspondiente */
    scanf("%d", &num1);

    printf("Escriba el segundo numero entero: ");
    scanf("%d", &num2);

    printf("Escriba el tercer numero entero: ");
    scanf("%d", &num3);

    /* utilizando "||" me va a decir que si tengo una comparacion que cumpla con el proposito aparecera el numero 1 que es verdadero*/

    int resultado = num1 >= num2 || num2 > num3;

    printf("Si tu primer numero es mayor o igual que el segundo, y tambien si tu segundo numero es mayor al tercero, si cumple con una comparacion verdadera, saldra 1: %i \n \n", resultado);

    /* Aqui coloque que se escriban los otros dos numeros paara cumplir con el operaador logico AND */

    printf("Escriba el cuarto numero entero: ");
    scanf("%d", &num4);

    printf("Escriba el quinto numero entero: ");
    scanf("%d", &num5);

    /* con la doble "&&" estoy uniendo ambos operaciones, si todas las comparaciones cumplen el proposito aparecera un 1*/

    int resultado1 = num3 < num4 && num4 <= num5;

    printf("Si el tercer numero es menor al cuarto y si el cuarto es menor o igual aal quinto, teniendo ambas iguales, saldra 1 (es verdadero) o 0 (si es falso): %i \n \n", resultado1);


    return 0; /* finaliza el ptrograma */
}
